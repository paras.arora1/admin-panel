const Navbar = () => {
  return (
    <header className="border-b py-4 mb-4">
      <div className="container mx-auto px-4">
        <div className="flex justify-between">
          <h1 className="font-semibold text-lg">Admin Panel</h1>
          <div>
            <p>Home</p>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
