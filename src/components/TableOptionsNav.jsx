const SortOptions = ["None", "Price", "Stock", "Rating"];
const FilterOptions = [
  { id: "filter-0", name: "None", filter: "" },
  {
    id: "filter-1",
    name: "smartphones",
    filter: "/category/smartphones",
  },
  {
    id: "filter-2",
    name: "laptops",
    filter: "/category/laptops",
  },
  {
    id: "filter-3",
    name: "furniture",
    filter: "/category/furniture",
  },
  {
    id: "filter-4",
    name: "mens-shoes",
    filter: "/category/mens-shoes",
  },
  {
    id: "filter-5",
    name: "womens-bags",
    filter: "/category/womens-bags",
  },
];

const TableOptionsNav = ({
  sortBy,
  selectedFilter,
  handleSortByChange,
  handleFilterChange,
  openModal,
}) => {
  return (
    <div className="mb-4 flex">
      <div className="flex items-center mr-8">
        <label htmlFor="filter-options" className="mr-4">
          Filters:
        </label>
        <select
          name="filter-options"
          id="filter-options"
          value={selectedFilter}
          onChange={(e) => handleFilterChange(e.target.value)}
          className="bg-white border rounded-md px-2 py-2"
        >
          {FilterOptions.map((filterOption) => (
            <option key={filterOption.id} value={filterOption.filter}>
              {filterOption.name}
            </option>
          ))}
        </select>
      </div>
      <div className="sorting">
        <label htmlFor="sort-options" className="mr-4">
          Sort By:
        </label>
        <select
          name="sort-options"
          id="sort-options"
          value={sortBy}
          onChange={(e) => handleSortByChange(e.target.value)}
          className="bg-white border rounded-md px-2 py-2"
        >
          {SortOptions.map((sortOption) => (
            <option key={sortOption} value={sortOption}>
              {sortOption}
            </option>
          ))}
        </select>
      </div>
      <div className="search ml-auto">
        <button
          onClick={openModal}
          className="rounded-lg px-4 py-2 text-white bg-blue-400 font-semibold"
        >
          Add Product
        </button>
      </div>
    </div>
  );
};

export default TableOptionsNav;
