import { useEffect, useState } from "react";

const getProducts = async (filter, page) => {
  const skip = page * 3;
  const res = await fetch(
    `https://dummyjson.com/products${filter}?limit=3${
      skip ? "&skip=" + skip : ""
    }`
  );

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
};

const Headers = ["Image", "Title", "Price", "Rating", "Stock", "Category"];

function Table({
  sortBy = "",
  selectedFilter = "",
  page = 0,
  handlePageChange,
  newProducts,
}) {
  const [products, setProducts] = useState([]);
  const [totalProducts, setTotalProducts] = useState();

  useEffect(() => {
    const fetchProducts = async () => {
      let { products, total } = await getProducts(selectedFilter, page);

      products = [...newProducts, ...products];
      if (sortBy) {
        if (sortBy === "Price") {
          products.sort((a, b) => b.price - a.price);
        } else if (sortBy === "Rating") {
          products.sort((a, b) => b.rating - a.rating);
        } else if (sortBy === "Stock") {
          products.sort((a, b) => b.stock - a.stock);
        }
      }

      setProducts(products);
      setTotalProducts(total);
    };

    fetchProducts();
  }, [sortBy, selectedFilter, page, newProducts]);

  const renderPagination = () => {
    const arr = [];
    for (let i = 0; i < Math.min(Math.ceil(totalProducts / 3), 10); i++) {
      arr.push(
        <button
          key={`page-${i}`}
          className={`border rounded-lg w-12 h-12 flex items-center justify-center ${
            page === i ? "bg-blue-400 text-white" : ""
          }`}
          onClick={() => {
            console.log(i);
            handlePageChange(i);
          }}
        >
          {i + 1}
        </button>
      );
    }

    return arr;
  };

  return (
    <div>
      <div className="headers flex border-t border-b py-4 bg-slate-50">
        {Headers.map((header) => (
          <p key={header} className="flex-1 text-center font-semibold">
            {header}
          </p>
        ))}
      </div>
      <div className="table-rows">
        {products.map((product) => (
          <div key={product.id} className="row flex py-4 border-b">
            <div className="flex-1 flex justify-center items-center">
              <img src={product.thumbnail} width="80" />
            </div>
            <div className="flex-1 flex justify-center items-center">
              <p className="text-center">{product.title}</p>
            </div>
            <div className="flex-1 flex justify-center items-center">
              <p className="text-center">{product.price}</p>
            </div>
            <div className="flex-1 flex justify-center items-center">
              <p className="text-center">{product.rating}</p>
            </div>
            <div className="flex-1 flex justify-center items-center">
              <p className="text-center">{product.stock}</p>
            </div>
            <div className="flex-1 flex justify-center items-center">
              <p className="text-center">{product.category}</p>
            </div>
          </div>
        ))}
      </div>
      <div className="flex justify-center mt-8">
        <div className="flex gap-2">{renderPagination()}</div>
      </div>
    </div>
  );
}

export default Table;
