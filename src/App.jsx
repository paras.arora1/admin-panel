import { useState } from "react";
import Container from "./components/Container";
import Navbar from "./components/Navbar";
import TableOptionsNav from "./components/TableOptionsNav";
import Table from "./components/Table";

function App() {
  const [sortBy, setSortBy] = useState("");
  const [selectedFilter, setSelectedFilter] = useState("");
  const [page, setPage] = useState(0);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [formValues, setFormValues] = useState({
    thumbnail: "https://www.pngkey.com/png/detail/233-2332677_ega-png.png",
    title: "",
    category: "smartphones",
    price: "",
    rating: "",
    stock: "",
  });
  const [newProducts, setNewProducts] = useState([]);

  const handleSortByChange = (val) => {
    setSortBy(val);
  };

  const handleFilterChange = (val) => {
    setSelectedFilter(val);
    setPage(0);
  };

  const handlePageChange = (num) => {
    setPage(num);
  };

  const handleAddProduct = async (e) => {
    e.preventDefault();
    console.log({ formValues });

    const res = await fetch("https://dummyjson.com/products/add", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formValues),
    });

    const data = await res.json();

    setNewProducts([...newProducts, data]);
    setIsModalVisible(false);
  };

  return (
    <div>
      <Navbar />
      <div>
        <Container className="mb-8">
          <h1 className="text-2xl font-semibold text-center">
            Product Management
          </h1>
        </Container>

        <Container>
          <TableOptionsNav
            sortBy={sortBy}
            selectedFilter={selectedFilter}
            handleSortByChange={handleSortByChange}
            handleFilterChange={handleFilterChange}
            openModal={() => setIsModalVisible(true)}
          />
          <Table
            sortBy={sortBy}
            selectedFilter={selectedFilter}
            page={page}
            handlePageChange={handlePageChange}
            newProducts={newProducts}
          />
        </Container>
      </div>
      {isModalVisible && (
        <div
          className="absolute inset-0 flex justify-center items-center"
          style={{ background: "rgba(0, 0, 0, 0.2)" }}
        >
          <form
            className="w-80 bg-white rounded-lg p-4"
            onSubmit={handleAddProduct}
          >
            <h1 className="text-center font-semibold text-xl mb-4">
              Add Product
            </h1>
            <input
              type="text"
              placeholder="Product Title"
              value={formValues.title}
              onChange={(e) =>
                setFormValues({ ...formValues, title: e.target.value })
              }
              className="px-3 py-2 border rounded-lg mb-4"
            />
            <input
              type="number"
              placeholder="Product Price"
              value={formValues.price}
              onChange={(e) =>
                setFormValues({ ...formValues, price: e.target.value })
              }
              className="px-3 py-2 border rounded-lg mb-4"
            />
            <input
              type="number"
              placeholder="Current Stock"
              value={formValues.stock}
              onChange={(e) =>
                setFormValues({ ...formValues, stock: e.target.value })
              }
              className="px-3 py-2 border rounded-lg mb-4"
            />
            <input
              type="number"
              placeholder="Rating"
              value={formValues.rating}
              onChange={(e) =>
                setFormValues({ ...formValues, rating: e.target.value })
              }
              className="px-3 py-2 border rounded-lg mb-4"
            />
            <div className="">
              <label htmlFor="category" className="block">
                Select Category
              </label>
              <select
                name="category"
                id="category"
                value={formValues.category}
                onChange={(e) =>
                  setFormValues({ ...formValues, category: e.target.value })
                }
                className="border rounded-lg bg-white px-3 py-2 block mt-1 w-full"
              >
                {[
                  "smartphones",
                  "laptops",
                  "furniture",
                  "mens-shoes",
                  "womens-bags",
                ].map((category) => (
                  <option value={category} key={category}>
                    {category}
                  </option>
                ))}
              </select>
            </div>
            <div className="flex justify-center gap-2 mt-8">
              <button
                type="submit"
                className="px-4 py-2 font-semibold bg-blue-400 rounded-lg text-white"
              >
                Submit
              </button>
              <button
                type="button"
                onClick={() => setIsModalVisible(false)}
                className="px-4 py-2 font-semibold bg-red-400 rounded-lg text-white"
              >
                Close
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}

export default App;
